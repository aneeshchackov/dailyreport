package com.ustglobal.itopsreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ITopsReportMainApp {

	public static void main(String[] args) {
		SpringApplication.run(ITopsReportMainApp.class, args);
	}
}
