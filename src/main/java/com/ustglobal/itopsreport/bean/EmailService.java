package com.ustglobal.itopsreport.bean;



import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.opencsv.exceptions.CsvValidationException;
public class EmailService 
{
	@Autowired
	 private JavaMailSender mailSender;
	   private SimpleMailMessage simpleMailMessage;
	    
	    public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
	        this.simpleMailMessage = simpleMailMessage;
	    }

	    public void setMailSender(JavaMailSender mailSender) {
	        this.mailSender = mailSender;
	    }
	    
	    public void attachFileToExchange()   {
	    
	       MimeMessage message = mailSender.createMimeMessage();
	        
	       try{
	    	  
	        MimeMessageHelper helper = new MimeMessageHelper(message, true);
	  
	        helper.setFrom("svc_hr_mailrelay@humanaedge.com");
	        helper.setTo("v-Saravana.Perumal@authorbyhumana.com");
	        helper.setSubject("HRP-MEMBER & PROVIDER  Preference Audit Count Details");
	        helper.setText("PLEASE FIND THE MEMBER AND PROVIDER COUNT DETAILS ON THE ABOVE ATTACHED FILE");
            
	       FileSystemResource file1 = new FileSystemResource("D:/QueryReport/Member_Provider/provider_query.csv");
	       System.out.println(file1);
	        helper.addAttachment(file1.getFilename(), file1);
	        
	        FileSystemResource file2 = new FileSystemResource("D:/QueryReport/Member_Provider/member_query.csv");
	        System.out.println(file2);
	        helper.addAttachment(file2.getFilename(), file2);
	               
	         }
	       catch (MessagingException e) {
	        throw new MailParseException(e);
	         }
	         mailSender.send(message);
	     
	         }}
	