package com.ustglobal.itopsreport.bean;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public final class AuditWireTapProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		exchange.getIn().setBody(null);
	}
}
